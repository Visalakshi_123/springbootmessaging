package com.allstate.messaging;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class MqReceiver {
    @Value(value = "${queuename}")
    private String queuename;

//    @JmsListener(destination = "dgqueue")
//public void receiveQueueMessage(String message){
//        System.out.println(message);
//
//    }

    @JmsListener(destination = "visatopic" ,containerFactory = "myFactoryTopic")
    public void receiveTopicMessage(String message){
        System.out.println(message);

    }


}
